#ifndef CUSTOM_JOINT_H
#define CUSTOM_JOINT_H

#include <cyclone\joints.h>

class CustomJoint : public cyclone::Joint {
private:
    cyclone::Vector3 _worldPosition;
public:
	CustomJoint( cyclone::RigidBody *a, cyclone::Vector3 pos ) {
		this->body[0] = a;
		this->position[0] = pos;
		CustomJoint::error = 0.0f;
	}

	void setWorldPosition( cyclone::Vector3 point ) {
		this->_worldPosition = point;
	}
	unsigned addContact( cyclone::Contact *contact, unsigned limit ) const;
 };
#endif