#include "dice.h"

class DiceSixSided : public Dice {
public:
    DiceSixSided( );
    ~DiceSixSided( void );

    void render( bool debug );
	void renderShadow( void );
    void update( cyclone::real duration );
    void collisionPlane( cyclone::CollisionPlane plane, cyclone::CollisionData *collisionData );
    void setState( cyclone::real x, cyclone::real y, cyclone::real z );
	unsigned collisionDiceAndHalfSpace( const cyclone::CollisionBox &box, const cyclone::CollisionSphere &sphere, const cyclone::CollisionPlane &plane, cyclone::CollisionData *data );
};