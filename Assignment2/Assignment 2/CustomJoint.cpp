#include "CustomJoint.h"

unsigned CustomJoint::addContact( cyclone::Contact *contact, unsigned limit ) const {
    // Calculate the position of each connection point in world coordinates
    cyclone::Vector3 a_pos_world = body[0]->getPointInWorldSpace(position[0]);
    cyclone::Vector3 b_pos_world = this->_worldPosition;

    // Calculate the length of the joint
    cyclone::Vector3 a_to_b = b_pos_world - a_pos_world;
    cyclone::Vector3 normal = a_to_b;
    normal.normalise();
    cyclone::real length = a_to_b.magnitude();

    // Check if it is violated
    if (real_abs(length) > 0) {
        contact->body[0] = body[0];
		contact->body[1] = 0;
        contact->contactNormal = normal;
        contact->contactPoint = this->_worldPosition;
        contact->penetration = length;
        contact->friction = 1.0f;
        contact->restitution = 0;
		// Add throw effect
		body[0]->addVelocity(a_to_b*4);
        return 1;
    }

    return 0;
}