#ifndef DICE_H
#define DICE_H

#include <gl/glut.h>
#include <cyclone/cyclone.h>

class Dice : public cyclone::CollisionBox {
public:
	cyclone::CollisionSphere *cSphere;

    Dice(  ) {
        this->body = new cyclone::RigidBody;
        this->cSphere = new cyclone::CollisionSphere();
        this->cSphere->body = new cyclone::RigidBody();
    }

    virtual ~Dice( void ) {
        delete this->body;
        delete this->cSphere;
    }

    virtual void render( bool debug ) = 0;
	virtual void renderShadow( void ) = 0;
    virtual void update( cyclone::real duration ) = 0;
    virtual void collisionPlane( cyclone::CollisionPlane plane, cyclone::CollisionData *collisionData ) = 0;
    virtual void setState( cyclone::real x, cyclone::real y, cyclone::real z ) = 0;
	void collisionDice( Dice *dice, cyclone::CollisionData *collisionData ){
		if( cyclone::IntersectionTests::boxAndBox( *this, *dice ) ){
			cyclone::CollisionDetector::boxAndBox( *this, *dice, collisionData );
		}
	}
};
#endif