#include <gl/glut.h>
#include <cyclone/cyclone.h>
#include "../src/demos/app.h"
#include "../src/demos/timing.h"
#include "dice.h"
#include "DiceEightSided.h"
#include "DiceSixSided.h"
#include "CustomJoint.h"

#include <list>
#include <stdio.h>

static bool DEBUG_DRAW = false;
static bool WIRE_FRAME = false;

Application* getApplication( void );

typedef struct {
    cyclone::Vector3 o, d;
} Ray;

bool RayBoxIntersection( Ray ray, cyclone::CollisionBox box, cyclone::real &t ) {
    cyclone::Vector3 o = box.body->getPointInLocalSpace( ray.o );
    cyclone::Vector3 d = box.body->getDirectionInLocalSpace( ray.d );

    cyclone::Vector3 tmin = cyclone::Vector3( (cyclone::Vector3() - box.halfSize - o).x / d.x, (cyclone::Vector3() - box.halfSize - o).y / d.y, (cyclone::Vector3() - box.halfSize - o).z / d.z );
	cyclone::Vector3 tmax = cyclone::Vector3( (box.halfSize - o).x / d.x, (box.halfSize - o).y / d.y, (box.halfSize - o).z / d.z );
    cyclone::Vector3 temp = tmin;

    tmin = cyclone::Vector3( temp.x < tmax.x ? temp.x : tmax.x, temp.y < tmax.y ? temp.y : tmax.y, temp.z < tmax.z ? temp.z : tmax.z );
    tmax = cyclone::Vector3( temp.x > tmax.x ? temp.x : tmax.x, temp.y > tmax.y ? temp.y : tmax.y, temp.z > tmax.z ? temp.z : tmax.z );

    cyclone::real time = tmin.x;
    time = time > tmin.y ? time : tmin.y;
    time = time > tmin.z ? time : tmin.z;

    cyclone::real last = tmax.x;
    last = last < tmax.y ? last : tmax.y;
    last = last < tmax.z ? last : tmax.z;

    t = time;
    return time <= last;
}

class Assignment2 : public RigidBodyApplication {
private:
    std::list<Dice*> _dices;
	Dice *_dragDice;
    bool _isDragging;
	CustomJoint *_dragJoint;
	cyclone::real _dragTime;

	// Build the contacts for the current situation
    virtual void generateContacts( void );
    // Processes the objects in the simulation forward in time
    virtual void updateObjects( cyclone::real duration );
	// Update the Application
	virtual void update();
	// Reset the application
	virtual void reset();

public:
    Assignment2( void );
    virtual ~Assignment2( void );
    // Returns the window title
    virtual const char* getTitle( void );
    // Initializes graphics
    virtual void initGraphics( void );
    // Do GL picking
    virtual void select( int x, int y );
    // Display the world
    virtual void display( void );
    // Handle keypress
    virtual void key( unsigned char key );
    // Handles mouse actions
    virtual void mouse( int button, int state, int x, int y );
    // Handles mouse drag
    virtual void mouseDrag( int x, int y );
};

Assignment2::Assignment2( ) : RigidBodyApplication() {
    Dice *dice;
	pauseSimulation = false;
    this->_isDragging = false;
	this->_dragJoint = NULL;
	this->_dragDice = NULL;

	for( int i = 0; i < 5; ++i ) {
		this->_dices.push_back( dice = new DiceSixSided() );
		dice->setState( i, 5*i, i );
	}
	for( int j = 0; j < 5; ++j ) {
		this->_dices.push_back( dice = new DiceEightSided() );
		dice->setState( j, 5*j, j );
	}
}

static bool deleteDice( Dice *dice ) {
    delete dice; return true;
}

Assignment2::~Assignment2() {
    this->_dices.remove_if( deleteDice );
}

void Assignment2::display( void ) {
    const static GLfloat lightPosition[] = {-1, 1, 0, 0};
    
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glLoadIdentity();
	
	// Camera Position
    gluLookAt( 0.0, 20.0, 40.0, 0.0, 5.0, 0.0, 0.0, 1.0, 0.0 );

	// Plane
	glColor3f( 0.75, 0.75, 0.75 );
	glBegin(GL_POLYGON);
	glVertex3f(-20,0,-20);
    glVertex3f(20,0,-20);
	glVertex3f(-20,0,20);
    glVertex3f(20,0,20);
	glVertex3f(20,0,-20);
	glEnd();

    
	// Render shadows
    glEnable( GL_BLEND );
    glColor4f( 0.0, 0.0, 0.0, 0.1 );
    glDisable( GL_DEPTH_TEST );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    std::list<Dice*>::const_iterator dice;
    for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
        (*dice)->renderShadow();
    }

    glDisable( GL_BLEND );

    // Render Dice
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_LIGHTING );
    glLightfv( GL_LIGHT0, GL_POSITION, lightPosition );
    glColorMaterial( GL_FRONT_AND_BACK, GL_DIFFUSE );
    glEnable( GL_COLOR_MATERIAL );
    glColor3f( 1.0, 1.0, 1.0 );
    
    if( WIRE_FRAME ) {
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }

    for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
		(*dice)->render(DEBUG_DRAW);
    }

    glDisable( GL_COLOR_MATERIAL );
    glDisable( GL_LIGHTING );
    glDisable( GL_DEPTH_TEST );



    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    // Render some text
    glColor3f( 0.0, 0.0, 0.0 );
    this->renderText( 10.0, 580.0, "Click on the dice to pick it up." );
}

const char *Assignment2::getTitle() {
    return "Assignment 2 Dice by Roy Springer 500616692";
}

void Assignment2::initGraphics( void )
{
    GLfloat lightAmbient[] = {0.8f, 0.8f, 0.8f, 1.0f};
    GLfloat lightDiffuse[] = {0.9f, 0.95f, 1.0f, 1.0f};

    glLightfv( GL_LIGHT0, GL_AMBIENT, lightAmbient );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, lightDiffuse );

    glEnable( GL_LIGHT0 );

    Application::initGraphics();
}

void Assignment2::select( int x, int y ) {
    GLdouble model[16], proj[16];
    GLint view[4];

    GLdouble oX, oY, oZ, eX, eY, eZ;

    glGetDoublev( GL_MODELVIEW_MATRIX, model );
    glGetDoublev( GL_PROJECTION_MATRIX, proj );
    glGetIntegerv( GL_VIEWPORT, view );

	gluUnProject( x, view[3] - y, 0.0, model, proj, view, &oX, &oY, &oZ ) != GLU_FALSE;
	gluUnProject( x, view[3] - y, 1.0, model, proj, view, &eX, &eY, &eZ ) != GLU_FALSE;

    Ray r;
    r.o = cyclone::Vector3( oX, oY, oZ );
	r.d = cyclone::Vector3( eX - oX, eY - oY, eZ - oZ );
	r.d.normalise();

    std::list<Dice*>::const_iterator dice;
    for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice )  {
       
		if( RayBoxIntersection( r, *(*dice), this->_dragTime ) ){
            cyclone::Vector3 pos = r.o + r.d * this->_dragTime;
            cyclone::Vector3 bpos = (*dice)->body->getPosition();
			
			this->_isDragging = true;
            this->_dragDice = *dice;
			CustomJoint *p = new CustomJoint( (*dice)->body, bpos - pos );
            this->_dragJoint = p;

            break;
        }
    }
}

void Assignment2::mouse( int button, int state, int x, int y ) {
	// Left button
	if( (button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN) ) {
		this->select( x, y );
	} else if( (button == GLUT_LEFT_BUTTON) && (state == GLUT_UP) ) {
		this->_isDragging = false;
		this->_dragDice = 0;
		
		if( this->_dragJoint != NULL ) {
			delete this->_dragJoint;
			this->_dragJoint = NULL;
		}
	}
}

void Assignment2::mouseDrag( int x, int y ) {
	// Dragging Mouse
	if( _dragDice ) {
		GLdouble model[16], proj[16];
		GLint view[4];

		GLdouble oX, oY, oZ, eX, eY, eZ;

		glGetDoublev( GL_MODELVIEW_MATRIX, model );
		glGetDoublev( GL_PROJECTION_MATRIX, proj );
		glGetIntegerv( GL_VIEWPORT, view );

		gluUnProject( x, view[3] - y, 0.0, model, proj, view, &oX, &oY, &oZ ) != GLU_FALSE;
		gluUnProject( x, view[3] - y, 1.0, model, proj, view, &eX, &eY, &eZ ) != GLU_FALSE;

		Ray r;
		r.o = cyclone::Vector3( oX, oY, oZ );
		r.d = cyclone::Vector3( eX - oX, eY - oY, eZ - oZ );
		r.d.normalise();

		cyclone::Vector3 pos = r.o + r.d * this->_dragTime;
		this->_dragJoint->setWorldPosition( pos );
	}
}

void Assignment2::generateContacts( void ) {
    // Create a ground plane
    cyclone::CollisionPlane plane;
    plane.direction = cyclone::Vector3( 0, 1, 0 );
    plane.offset = 0;

	// Create Walls
	cyclone::CollisionPlane wall1;
    wall1.direction = cyclone::Vector3( -1, 0, 0 );
    wall1.offset = -20;
	cyclone::CollisionPlane wall2;
	wall2.direction = cyclone::Vector3( 1, 0, 0 );
	wall2.offset = -20;
	cyclone::CollisionPlane wall3;
    wall3.direction = cyclone::Vector3( 0, 0, 1 );
    wall3.offset = -20;
	cyclone::CollisionPlane wall4;
    wall4.direction = cyclone::Vector3( 0, 0, -1 );
    wall4.offset = -20;
	
	this->cData.reset( RigidBodyApplication::maxContacts );
    this->cData.friction = (cyclone::real) 0.9;
    this->cData.restitution = (cyclone::real) 0.2;
    this->cData.tolerance = (cyclone::real) 0.1;

	// Add the joint contact if dragging a dice
	if( _dragDice != NULL ) {
		this->cData.addContacts( this->_dragJoint->addContact( this->cData.contacts, this->cData.contactsLeft ) );
	}

	// Update Collision of the dices
    std::list<Dice*>::const_iterator dice, dice2;
    for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
		if(!cData.hasMoreContacts()) return;

		(*dice)->collisionPlane( plane, &this->cData );
		(*dice)->collisionPlane( wall1, &this->cData );
		(*dice)->collisionPlane( wall2, &this->cData );
		(*dice)->collisionPlane( wall3, &this->cData );
		(*dice)->collisionPlane( wall4, &this->cData );
		
		for( dice2 = this->_dices.begin() ; dice2 != this->_dices.end() ; ++dice2 ) {
            if( (*dice2) != (*dice) ) {
				(*dice)->collisionDice( *dice2, &this->cData);
            }
        }
    }
}

void Assignment2::update(){
	RigidBodyApplication::update();
}

void Assignment2::updateObjects( cyclone::real duration ) {
	// Update all the dices
    std::list<Dice*>::const_iterator dice;
    for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
        (*dice)->update( duration );
    }
}

void Assignment2::reset( void ){

}

void Assignment2::key( unsigned char key ) {
	std::list<Dice*>::const_iterator dice;
    switch( key ) {
        case 'D': case 'd':
            DEBUG_DRAW = !DEBUG_DRAW;
            break;
        case 'W': case 'w':
            WIRE_FRAME = !WIRE_FRAME;
            break;
		case 'S': case 's':
			for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
				(*dice)->body->addVelocity(cyclone::Vector3(5,10,8));
			}
            break;
		case 'A': case 'a':
			for( dice = this->_dices.begin() ; dice != this->_dices.end() ; ++dice ) {
				(*dice)->body->addRotation(cyclone::Vector3(0,-1,1));
			}
            break;
    }

	RigidBodyApplication::key( key );
}

Application* getApplication( void ) {
    return new Assignment2();
}