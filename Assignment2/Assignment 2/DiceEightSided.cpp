#include "DiceEightSided.h"

DiceEightSided::DiceEightSided( void ) { 
	this->body = new cyclone::RigidBody();
    this->cSphere = new cyclone::CollisionSphere;
    this->cSphere->body = new cyclone::RigidBody();
}

DiceEightSided::~DiceEightSided( void ) {
    delete this->body;
    delete this->cSphere;
}

void DiceEightSided::render( bool debug ) {
	// Get the OpenGL transformation
	GLfloat mat[16];
	this->body->getGLTransform( mat );

	glPushMatrix();
	glMultMatrixf(mat);
	glScalef( halfSize.x, halfSize.y, halfSize.z );
	glutSolidOctahedron();
	//sqSolidDoublePyramid( this->cSphere->radius, 30, 20 );
	glPopMatrix();
	
	if(debug){
		cSphere->body->getGLTransform(mat);
		glColor3f(1, 0, 0);
		glPushMatrix();
		cyclone::Vector3 pos = cSphere->body->getPosition();
		glTranslatef(pos.x, pos.y, pos.z);
		glutWireSphere(cSphere->radius, 30, 30);
		glPopMatrix();
	}
}

void DiceEightSided::renderShadow( void ) {
	GLfloat mat[16];
	body->getGLTransform( mat );
	
	glPushMatrix();
	glScalef( 1.0, 0, 1.0 );
	glMultMatrixf( mat );
	glScalef( halfSize.x, halfSize.y, halfSize.z );
	glutSolidOctahedron();
	glPopMatrix();
}

void DiceEightSided::update( cyclone::real duration ) {
	this->body->integrate( duration );
	this->calculateInternals();
	
	this->cSphere->body->integrate(duration);
	this->cSphere->calculateInternals();

	// update the  position of the collision sphere
	this->cSphere->body->setPosition( this->body->getPosition() );
}


void DiceEightSided::setState( cyclone::real x, cyclone::real y, cyclone::real z ) {
	// Creating Body
	this->body->setPosition( x, y, z );
	this->body->setOrientation( 1, 0, 0, 0 );
	this->body->setRotation(1 ,1, 1);
	this->halfSize = cyclone::Vector3( 2, 2, 2 );
	
	this->halfSize.x == this->halfSize.y && this->halfSize.y == this->halfSize.z;
	
	cyclone::real mass = this->halfSize.x * this->halfSize.y * this->halfSize.z * 8.0f;
	this->body->setMass( mass );
	
	cyclone::Matrix3 tensor;
	tensor.setBlockInertiaTensor( this->halfSize, mass );
	this->body->setInertiaTensor( tensor );
	
	this->body->setDamping( 0.9, 0.8 );
	this->body->clearAccumulators();
	this->body->setAcceleration( 0, -9.81, 0 );
	
	this->body->setCanSleep( false );
	this->body->setAwake();
	this->body->calculateDerivedData();
	
	// Creating Collision Sphere
	this->cSphere->body = this->body;
	this->cSphere->radius = this->halfSize.x * .99;
	this->cSphere->body->calculateDerivedData();
}

void DiceEightSided::collisionPlane( cyclone::CollisionPlane plane, cyclone::CollisionData *collisionData ) {
	if( cyclone::IntersectionTests::boxAndHalfSpace( *this, plane ) ){
		eightSideCollision( *this, *this->cSphere, plane, collisionData );
	}
}

unsigned DiceEightSided::eightSideCollision( const cyclone::CollisionBox &box, const cyclone::CollisionSphere &sphere, const cyclone::CollisionPlane &plane, cyclone::CollisionData *data ) {
	// Make sure we have contacts
    if (data->contactsLeft <= 0) return 0;

    // We have an intersection, so find the intersection points. We can make
    // do with only checking vertices. If the box is resting on a plane
    // or on an edge, it will be reported as four or two contact points.

	// Go through each combination of + and - for each half-size
    cyclone::real mults[7][3] = {
		    { 0,	0, -1},
			{-1,	0,	0},
			{ 0,    0,	1},
			{ 0,   -1,	0},
			{ 1,	0,	0},
			{ 0,	1,	0},
			{ 0,	0,	0}
	};

	cyclone::Contact* contact = data->contacts;
    unsigned contactsUsed = 0;
    for (unsigned i = 0; i < 7; i++) {

		// Calculate the position of each vertex
		cyclone::Vector3 vertexPos(mults[i][0], mults[i][1] , mults[i][2] );
		vertexPos.componentProductUpdate(box.halfSize);
		vertexPos = box.getTransform().transform(vertexPos);

        // Calculate the distance from the plane
        cyclone::real vertexDistance = vertexPos * plane.direction;
		// Cache the sphere position
		cyclone::Vector3 position = sphere.getAxis(3);

		// Find the distance from the plane
		cyclone::real ballDistance =
		    plane.direction * position -
		    sphere.radius - plane.offset;

        // Compare this to the plane's distance
        if (vertexDistance <= plane.offset ) {

			if( vertexDistance < ballDistance ){
				cyclone::CollisionDetector::sphereAndHalfSpace(sphere, plane, data);
		
			}else if( ballDistance < vertexDistance ) {
				// Create the contact data.

				// The contact point is halfway between the vertex and the
				// plane - we multiply the direction by half the separation
				// distance and add the vertex location.
				contact->contactPoint = plane.direction;
				contact->contactPoint *= (vertexDistance-plane.offset);
				contact->contactPoint = vertexPos;
				contact->contactNormal = plane.direction;
				contact->penetration = plane.offset - vertexDistance;

				// Write the appropriate data
				contact->setBodyData(box.body, NULL,
				    data->friction, data->restitution);

				// Move onto the next contact
				contact++;
				contactsUsed++;
				if (contactsUsed == data->contactsLeft) return contactsUsed;
			
			}
        }
    }

    data->addContacts(contactsUsed);
    return contactsUsed;
}
