#include "dice.h"

class DiceEightSided : public Dice {
public:
    DiceEightSided(  );
    ~DiceEightSided( void );

    void render( bool debug );
	void renderShadow( void );
    void update( cyclone::real duration );
    void collisionPlane( cyclone::CollisionPlane plane, cyclone::CollisionData *collisionData );
    void setState( cyclone::real x, cyclone::real y, cyclone::real z );
	unsigned eightSideCollision( const cyclone::CollisionBox &box, const cyclone::CollisionSphere &sphere, const cyclone::CollisionPlane &plane, cyclone::CollisionData *data );
};