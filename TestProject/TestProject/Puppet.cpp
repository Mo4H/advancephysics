/*
 * The Puppet Assignement 1b
 * Created by Roy Springer 500616692
 */

/** include the engine headers */
#include <gl/glut.h>
#include <cyclone/cyclone.h>
/** include the demo framwork for easy setup the Project*/
#include "../../src/demos/app.h"
#include "../../src/demos/timing.h"

#include <stdio.h>
#include <cassert>

using namespace cyclone;

#define ROD_COUNT 16
#define CABLE_COUNT 3

#define BASE_MASS 1
#define EXTRA_MASS 10

/**
 * The main Puppet class definition.
 */
class Puppet : public MassAggregateApplication {

    ParticleCable	*cables;
    ParticleRod		*rods;
	ParticleBungee	*supportsBungee1;
	ParticleBungee	*supportsBungee2;
	ParticleBungee	*supportsBungee3;

	real supportRes;
	Vector3 supportPos;
	Vector3 supportPosL;
	Vector3 supportPosR;
	real supportLenght;
	real supportLenghtL;
	real supportLenghtR;

	bool leftPressed;
	bool rightPressed;
	bool headUp;
	bool headDown;      

	void createParticle(real x, real y, real z, int index);
	void createRodConnection(int indexRod, real length, int particleOneIndex, int particleTwoIndex);
	void createCableConnection(int indexCable, real length, int particleOneIndex, int particleTwoIndex);

	void updateSupports();

public:
    /** Creates a new puppet object. */
    Puppet();
    virtual ~Puppet();

    /** Returns the window title for the demo. */
    virtual const char* getTitle();

    /** Display the particles. */
    virtual void display();

    /** Update the particle positions. */
    virtual void update();

    /** Handle a key press. */
    virtual void key(unsigned char key);
};

// Method definitions
Puppet::Puppet() : MassAggregateApplication(16) {
	//Setting all the bool to false
	leftPressed		= false;
	rightPressed	= false;
	headUp			= false;
	headDown		= false;

    // Create the particles.
	for(int i = 0; i < 13; i++){
		createParticle(0.0f, 0.0f, 0.0f, i);
	}

	// ------------- Create the puppet ---------------
	rods = new ParticleRod[ROD_COUNT];
	cables = new ParticleCable[CABLE_COUNT];
	//Head
	createRodConnection( 0,  1.0f, 0, 1);
	//Neck / Upper Body
	createCableConnection( 0, 0.4f, 1, 2);
	createCableConnection( 1,  0.6f, 2, 3);
	createCableConnection( 2,  0.6f, 2, 4);
	createRodConnection( 1,  1.0f, 3, 4);
	//Left Arm
	createRodConnection( 2,  0.5f, 4, 5);
	createRodConnection( 3,  1.6f, 5, 8);
	createRodConnection( 4, 1.6f, 8, 4);
	//Right Arm
	createRodConnection( 5,  0.5f, 3, 6);
	createRodConnection( 6,  1.6f, 6, 7);
	createRodConnection( 7,  1.6f, 7, 3);
	// Lower Body
	createRodConnection( 8, 2.0f, 4, 9);
	createRodConnection( 9, 2.0f, 3, 10);
	createRodConnection( 10, 1.0f, 9, 10);
	//Left Leg
	createRodConnection( 11, 1.0f, 9, 11);
	//Rigt Leg
	createRodConnection( 12, 1.0f, 12, 10);
	//----------------------------------------------

	// ------------- Top three ropes ---------------
	supportRes = -50.0f;
	supportPos = Vector3( 0.0f, 7.5f, 0.8f );
	supportPosL = supportPos + Vector3( 1.2f, 0, 0.0f );
	supportPosR = supportPos + Vector3( -1.2f, 0, 0.0f );
	supportLenght = 0.1f;
	supportLenghtL = 0.3f;
	supportLenghtR = 0.3f;

	// Upper Three particles
	createParticle(0.0f,	1.0f, 0.8f, 13);
	createParticle(1.2f,	1.0f, 0.8f, 14);
	createParticle(-1.2f,	1.0f, 0.8f, 15);
	// Connection Particles
	createRodConnection(13, 1.0f, 14, 13);
	createRodConnection(14, 1.0f, 13, 15);
	createRodConnection(15, 2.0f, 14, 15);

	supportsBungee1 = new ParticleBungee(&particleArray[13], -30, supportLenght);
	supportsBungee2 = new ParticleBungee(&particleArray[14], -20, supportLenghtL);
	supportsBungee3 = new ParticleBungee(&particleArray[15], -20, supportLenghtR);

	world.getForceRegistry().add(&particleArray[0], supportsBungee1);
	world.getForceRegistry().add(&particleArray[6], supportsBungee2);
	world.getForceRegistry().add(&particleArray[5], supportsBungee3);
	//---------------------------------------------
}

void Puppet::createParticle(real x, real y, real z, int index){
	particleArray[index].setPosition( x, y, z );
	particleArray[index].setVelocity(0,0,0);
	particleArray[index].setDamping(1.0f);
	particleArray[index].setMass(BASE_MASS);
	if(index < 13){
		particleArray[index].setAcceleration(Vector3::GRAVITY);
	}
	particleArray[index].clearAccumulator();
}
/**
 * Easy funciton for creating rod connections
 */
void Puppet::createRodConnection(int indexRod, real length, int particleOneIndex, int particleTwoIndex){
	rods[indexRod].particle[0] = &particleArray[particleOneIndex];
    rods[indexRod].particle[1] = &particleArray[particleTwoIndex];
    rods[indexRod].length = length;
    world.getContactGenerators().push_back(&rods[indexRod]);
}
/**
 * Easy funciton for creating cable connections
 */
void Puppet::createCableConnection(int indexCable, real length, int particleOneIndex, int particleTwoIndex){
	cables[indexCable].particle[0] = &particleArray[particleOneIndex];
    cables[indexCable].particle[1] = &particleArray[particleTwoIndex];
    cables[indexCable].maxLength = length;
	cables[indexCable].restitution = 0.5f;
    world.getContactGenerators().push_back(&cables[indexCable]);
}

Puppet::~Puppet(){
    if (cables) delete[] cables;
    if (rods) delete[] rods;
}

void Puppet::display(){
    MassAggregateApplication::display();

    glBegin(GL_LINES);
    glColor3f(0,0,1);
    for (unsigned i = 0; i < ROD_COUNT; i++) {
        Particle **particles = rods[i].particle;
        const Vector3 &p0 = particles[0]->getPosition();
        const Vector3 &p1 = particles[1]->getPosition();
        glVertex3f(p0.x, p0.y, p0.z);
        glVertex3f(p1.x, p1.y, p1.z);
    }
	glColor3f(0,1,0);
	for (unsigned i = 0; i < CABLE_COUNT; i++) {
        Particle **particles = cables[i].particle;
        const Vector3 &p0 = particles[0]->getPosition();
        const Vector3 &p1 = particles[1]->getPosition();
        glVertex3f(p0.x, p0.y, p0.z);
        glVertex3f(p1.x, p1.y, p1.z);
    }
	
    glColor3f(1.0f, 0.0f, 0.0f);
	const Vector3 &p0 = particleArray[13].getPosition();
	const Vector3 &p1 = particleArray[0].getPosition();
    glVertex3f(p0.x, p0.y, p0.z);
    glVertex3f(p1.x, p1.y, p1.z);

	const Vector3 &p2 = particleArray[14].getPosition();
	const Vector3 &p3 = particleArray[6].getPosition();
    glVertex3f(p2.x, p2.y, p2.z);
    glVertex3f(p3.x, p3.y, p3.z);

	const Vector3 &p4 = particleArray[15].getPosition();
	const Vector3 &p5 = particleArray[5].getPosition();
    glVertex3f(p4.x, p4.y, p4.z);
    glVertex3f(p5.x, p5.y, p5.z);
    
    glEnd();

	// Render the description
    glColor3f(0.0f, 0.0f, 0.0f);
    renderText(5.0f, 50.0f, "Description:\nToggle Left: \'A\'\nToggle Right: \'D\'\nToggle Up: \'W\'\nToggle Down: \'S\'");
}

void Puppet::update() {
    MassAggregateApplication::update();
	updateSupports();
	//Adding velocity
	if(leftPressed){
		Vector3 velocity = particleArray[13].getVelocity();
		velocity += Vector3(-5.0f,0,0);
		particleArray[13].setVelocity(velocity);
	}else if(rightPressed){
		Vector3 velocity = particleArray[13].getVelocity();
		velocity += Vector3(5.0f,0,0);
		particleArray[13].setVelocity(velocity);
	}
	// Limit the top
	if(particleArray[13].getPosition().y < 8.0f){
		Vector3 velocity = particleArray[13].getVelocity();
		velocity += Vector3(0.0f, 2.0f,0);
		particleArray[13].setVelocity(velocity);
	}else{
		Vector3 position = particleArray[13].getPosition();
		position.y = 8.0f;
		particleArray[13].setPosition(position);
		headUp = false;
	}
	// Adding velocity up / down
	if(headUp){
		Vector3 velocity = particleArray[13].getVelocity();
		velocity += Vector3(0.0f, 4.0f,0);
		particleArray[13].setVelocity(velocity);
	}else if(headDown){
		Vector3 velocity = particleArray[13].getVelocity();
		velocity += Vector3(0.0f, -4.0f,0);
		particleArray[13].setVelocity(velocity);
	}
}
/**
 *  Updates the position and velocity reset
 */
void Puppet::updateSupports(){
	real positionY = particleArray[13].getPosition().y;
	for(int i = 13; i < 16; i++){
		particleArray[i].setVelocity( Vector3(0,0,0) );
		Vector3 currentPos = particleArray[i].getPosition();
		currentPos.y = positionY;
		currentPos.z = 0;
		particleArray[i].setPosition(currentPos);
	}
}

const char* Puppet::getTitle() {
    return "Assignment 1b > Puppet made By Roy Springer 500616692";
}

void Puppet::key(unsigned char key) {
    switch(key) {
    case 's': case 'S':
        if(headUp){
			headUp = false;
			headDown = false;
		}else{ headDown = !headDown; }
        break;
    case 'w': case 'W':
		if(headDown){
			headUp = false;
			headDown = false;
		}else{ headUp = !headUp; }
        break;
    case 'a': case 'A':
		if(rightPressed){
			leftPressed = false;
			rightPressed = false;
		}else{ leftPressed = !leftPressed; }
        break;
    case 'd': case 'D':
        if(leftPressed){
			leftPressed = false;
			rightPressed = false;
		}else{ rightPressed = !rightPressed; }
        break;
    default:
        MassAggregateApplication::key(key);
    }
	
}

/**
 * Called by the common demo framework to create an application
 * object (with new) and return a pointer.
 */
Application* getApplication(){
    return new Puppet();
}